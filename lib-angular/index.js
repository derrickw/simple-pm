var angular = require('angular');
var simplepmLib = require('simplepm');

var simplepm = angular.module('simplepm-angular', []);

simplepm.factory('simplepmCalculators', function() {
  return simplepmLib;
});

module.exports = simplepm;
