describe('server/lib/projects/index', function() {
  var projects;

  beforeEach(function () {
    projects = rrequire('server/lib/projects');
  });

  it('exists', function (done) {
    expect(projects).toBeDefined();
    projects.find({})
      .then(function(results) {
        console.log('find worked', results);
      })
      .catch(function(err) {
        console.log('find error', err);
      })
      .finally(done);
  });

  it('can save', function(done) {
    projects.save({ name: 'Something ' + new Date().getTime() })
      .then(function(results) {
        return projects.find()
      })
      .then(function(projectResults) {
        var first = projectResults[3];
        if (!first) return;

        first.name = first.name + ' UPDATED';
        return first.saveAsync()
      })
      .then(function() {
        done();
      })
      // .catch(function(err) {
      //   console.log('catch', err);
      //   done(new Error('bubbles'));
      // });
  })
});
