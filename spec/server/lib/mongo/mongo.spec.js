(function() {
  'use strict';

  var Promise;

  Promise = require('bluebird');

  describe('mongo', function() {

    var MongoConnect;

    describe('connect', function() {

      beforeEach(function () {
        MongoConnect = prequire('./server/lib/mongo/mongo', {
          mongodb: {
            MongoClient: {
              connect: function (connString, cb) {
                cb(null, { fake: 'connect' });
              }
            }
          }
        });
      });

      it('fails to connect without a db', function (done) {
        var rejected = false;
        MongoConnect()
          .catch(function () {
            rejected = true;
          })
          .finally(function () {
            expect(rejected).toBe(true);

            done();
          });
      });

      it('resolves connection object when connected', function (done) {
        var connection;

        MongoConnect({ db: 'dw' })
          .then(function (conn) {
            connection = conn;
          })
          .finally(function () {
            expect(connection._connection).toEqual(jasmine.objectContaining({ fake: 'connect' }));
            expect(typeof connection.collection).toBe('function');

            done()
          })


      })

    });

  });
})();
