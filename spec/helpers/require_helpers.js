(function(global) {

  var join, proxyquire;

  join = require('path').join;
  proxyquire = require('proxyquire');

  global.rrequire = function rrequire(modulePath) {
    return require(join(__dirname, '../..',  modulePath));
  }

  global.prequire = function prequire(modulePath, fakes) {
    return proxyquire(join(__dirname, '../..',  modulePath), fakes);
  }

})(global);
