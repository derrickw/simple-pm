var angular = require('angular');
var app = angular.module('simple-pm-data', []);

module.exports = app;

require('./local_data_service');
