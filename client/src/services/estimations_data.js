estimationsData.$inject = ['spmResourceFactory'];

module.exports = estimationsData;

function estimationsData(spmResourceFactory) {
  return spmResourceFactory({
    name: 'Estimation',
    url: '/api/estimations'
  });
}
