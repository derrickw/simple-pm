localData.$inject = ['$window', 'Promise', '_'];

module.exports = localData;

function localData($window, Promise, _) {
  let localData = {
    get: getData,
    set: setData
  };

  return localData;

  function getData(key) {
    let results = [];
    let singleValue = false;
    let data = null;

    if (_.isString(key)) {
      data = $window.simplepm.localData[key];
    }
    else if (_.isArray(key)) {
      data = _.map(key, function(k) {
        return $window.simplepm.localData[k];
      });
    }
    else {
      throw new Error('Invalid type for localData key.');
    }

    return Promise.resolve(data);
  }

  function setData(key, value) {
    $window.simplepm.localData[key] = value;
    return Promise.resolve(value);
  }
}
