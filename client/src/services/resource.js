spmResourceFactory.$inject = ['$http', '$httpParamSerializerJQLike'];

module.exports = spmResourceFactory;

function spmResourceFactory($http, paramsSerializer) {
  return function createReource(resourceFactoryOptions) {
    const RESOURCE_NAME = resourceFactoryOptions.name;
    const URL = resourceFactoryOptions.url;

    let { preStore } = resourceFactoryOptions;

    return {
      store: store,
      load: load,
      loadOne: loadOne
    };

    function store(obj) {
      obj = preStore ? preStore(obj) : obj;
      return (obj.id ? $http.put(URL, obj) : $http.post(URL, obj))
        .then(function handleStoreResults(results) {
          return results.data[0];
        })
        .catch(function handleStoreError(err) {
          alert(`Error storing ${RESOURCE_NAME}. Check console for details.`);
          console.log('store err', RESOURCE_NAME, err);
        });
    }

    function load(query) {
      var options = {
        params: { query: query },
        paramSerializer: '$httpParamSerializerJQLike'
      };

      return $http.get(URL, options)
        .then(function(results) {
          return results.data;
        })
        .catch(function(err) {
          alert(`Error loading ${RESOURCE_NAME}. Check console for details.`);
          console.log('load error', RESOURCE_NAME, err);
        });
    }

    function loadOne(id) {
      return $http.get(URL + '/' + id)
        .then(function(results) {
          return results.data;
        })
        .catch(function(err) {
          alert(`Error storing ${RESOURCE_NAME}. Check console for details.`);
          console.log('loadOne err', RESOURCE_NAME, err);
        });
    }
  }
}
