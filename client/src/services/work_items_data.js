import { map, pick, clone } from 'lodash';

workItemsData.$inject = ['spmResourceFactory'];

module.exports = workItemsData;

function workItemsData(spmResourceFactory) {
  return spmResourceFactory({
    name: 'WorkItem',
    url: '/api/work_items',

    preStore: (workItem) => {
      let workItemClone = clone(workItem);

      workItemClone.costItems = map(workItemClone.costItems, (costItem) => {
        // this responsibility probably belongs to the backend
        var ci = clone(costItem);
        ci.costItemBase = costItem.costItemBase._id;
        return ci;
      });

      return workItemClone;
    }
  });
}
