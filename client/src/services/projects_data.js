projectsData.$inject = ['_', 'spmLocalData', 'Promise', '$http'];

module.exports = projectsData;

function projectsData(_, spmLocalData, Promise, $http) {
  var projectsData = {
    loadOne: loadOne,
    load: load,
    store: store
  };

  return projectsData;

  function loadOne(id) {
    return projectsData.load()
      .then(function(projects) {
        var found = _.find(projects, function(proj) {
          return proj.id === id || proj._id === id;
        });

        if (!found) {
          throw new Error('Invalid project: ' + id);
        }

        return found;
      })
  }

  function load() {
    return $http.get('/api/projects')
      .then((results) => results.data)
      .catch((err) => console.log('err', err));
  }

  function store(project) {
    let url = '/api/projects';
    let request;

    return project._id ?
      $http.put(url, project) :
      $http.post(url, project);
  }

  // function store(stored) {
  //   return projectsData.load()
  //     .then(function(projects) {
  //       return stored.id ?
  //         updateProject(stored, projects) :
  //         addProject(stored, projects);
  //     })
  //     .catch(function(err) {
  //       console.log('Error storing project', err);
  //     });
  //
  //   function addProject(project, projects) {
  //     project.id = new Date().getTime();
  //     projects.push(project);
  //     return spmLocalData.set('projects', projects)
  //       .return(project);
  //   }
  // }
  //
  // function updateProject(stored, projects) {
  //   _.each(projects, function(proj, index) {
  //     if (proj.id === stored.id) {
  //       projects[index] = stored;
  //     }
  //   });
  //
  //   return spmLocalData.set('projects', projects);
  // }
}
