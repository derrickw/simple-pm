let app = require('angular').module('simple-pm-services', []);

module.exports = app;

app.factory('spmResourceFactory', require('./resource'));
app.factory('spmLocalData', require('./local_data'));
app.factory('spmProjectsData', require('./projects_data'));
app.factory('spmEstimationsData', require('./estimations_data'));
app.factory('spmWorkItemsData', require('./work_items_data'));
app.factory('spmCostItemsData', require('./cost_items_data'));
