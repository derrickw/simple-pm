costItemsData.$inject = ['spmResourceFactory'];

module.exports = costItemsData;

function costItemsData(spmResourceFactory) {
  return spmResourceFactory({
    name: 'CostItems',
    url: '/api/cost_items'
  });
}
