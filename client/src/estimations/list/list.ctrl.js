let _ = require('lodash');

const ESTIMATION_STATUSES = {
  DRAFT: 'DRAFT',
  ACTIVE: 'ACTIVE'
};

export default class EstimationsController {
  constructor(newEstimationModal, data, estimationsData) {
    console.log('data', data);
    // this._ = _;
    this._newEstimationModal = newEstimationModal;
    this._estimationsData = estimationsData;

    this.projects = data.projects;
    this.estimations = data.estimations;
  }

  showNewEstimationModal() {
    this._newEstimationModal.show()
      .then(handleModalResults.bind(this))
      .then(newEstimationResults.bind(this))

    function handleModalResults(newEstimation) {
      return this._estimationsData.store(newEstimation);
    }

    function newEstimationResults(results) {
      console.log('newEstimationResults', results);
    }

    function newEstimationError(err) {
      console.log('newEstimationError', err);
    }
  }
}

EstimationsController.$inject = [
  'newEstimationModal',
  'data',
  'spmEstimationsData'
];
