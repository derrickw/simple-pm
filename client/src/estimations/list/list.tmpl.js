module.exports = `
  <div class="box">
      <div class="box-header">
          <h3 class="box-title">Estimations</h3>
          <span class="pull-right"><button ng-click="vm.showNewEstimationModal()" class="btn btn-xs btn-primary">Add Estimation</button></span>
      </div>
      <div class="box-body">
          <div class="box box-primary pull-left" ng-repeat="estimation in vm.estimations" style="width: 31%; margin-right: 2%">
            <div class="box-header">
                <h3 class="box-title"><a ui-sref="main.estimations.details({ estimationId: estimation._id })">{{ estimation.name }}</a></h3>
                <div class="box-tools pull-right">
                  <div class="btn-group">
                      <!-- <button type="button" class="btn btn-xs btn-default"><i class="fa fa-ellipsis-h"></i></button> -->
                      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-gear"></i>
                          <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="pull-right dropdown-menu" role="menu">
                          <li><a href="#" class="" ng-click="vm.doButtonAction('edit', estimation.id)">Edit</a>
                          <li><a href="#" class="" ng-click="vm.doButtonAction('addEstimation', estimation.id)">Add Estimation</a></li>
                          <li><a ui-sref="main.projects.details({ projectId: estimation.id })"
                              class="">View Project</a></li>
                          <li><a href="#">Hide</a></li>
                      </ul>
                  </div>
                </div>
            </div>

      </div>
      <div class="box-footer clearfix">
      </div>
  </div>
`;
