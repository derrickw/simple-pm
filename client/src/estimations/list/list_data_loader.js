export default function estimationsListDataLoader(Promise, estimationsData) {
  return Promise.all([estimationsData.load()])
    .spread(handleEstimationsLoad)
    .catch(handleEstimationsLoadError);

  function handleEstimationsLoad(estimations, projects) {
    return { estimations, projects };
  }

  function handleEstimationsLoadError(err) {
    alert('Error loading estimations');
    console.log('estimationsData.load', err);
    throw err;
  }
};

estimationsListDataLoader.$inject = [
  'Promise',
  'spmEstimationsData'
];
