module.exports = `
  <h3>Work Item Count: {{vm.estimationWorkItems.length}}</h3>
  <form>
    <formly-form fields="vm.estimationForm" model="vm.estimation"></formly-form>
  </form>
  <hr />
  <div class="button-panel" style="overflow:hidden; margin-bottom: 15px;">
    <button class="btn btn-success" ng-click="vm.addWorkItem(vm.estimation)">Add Work Item</button>
  </div>

  <work-item readonly ng-repeat="workItem in vm.estimationWorkItems" item="workItem"></work-item>

  <hr />

  <!-- <div class="static-work-item">
    <strong>Name:</strong> Do Something
  </div>

   -->
`;
