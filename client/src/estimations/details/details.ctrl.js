import { clone } from 'lodash';
import formFields from './details_form_fields';

export default class EstimationDetailsController {
  constructor($state, data, workItems, costItems) {
    this.estimation = data.estimation;
    this.estimationWorkItems = data.estimationWorkItems;
    this.projects = data.projects;
    this.workItemsData = workItems;

    this.currentEstimation = clone(this.estimation);

    this.estimationForm = formFields(this);
  }

  addWorkItem(estimation) {

    this.workItemsData.store(defaultWorkItem(estimation))
      .then(function (storeResults) {
        this.estimationWorkItems.push(storeResults);
      }.bind(this));
  }
}

EstimationDetailsController.$inject = [
  '$state',
  'data',
  'spmWorkItemsData',
  'spmCostItemsData'
];

function defaultWorkItem(estimation) {
  var workItem = {
    estimation: estimation._id,
    name: formatDefaultName(estimation.name)
  };

  return workItem;
}

function formatDefaultName(estimationName) {
  return `${estimationName} - Work Item`;
}

function filterByType(items, type) {
  return _.filter(items, (item) => item.type === type);
}
