export default function estimationDetailsDataLoader(Promise, estimationsData, projectsData, workItems, $stateParams) {
  var dataLoads = [
    estimationsData.loadOne($stateParams.estimationId),
    projectsData.load(),
    workItems.load({ estimation: $stateParams.estimationId })
  ];

  return Promise.all(dataLoads)
    .spread(handleEstimationsLoad)
    .catch(handleEstimationsLoadError);

  function handleEstimationsLoad(estimation, projects, estimationWorkItems) {
    console.log('estimations data', estimation, projects);
    return { estimation, projects, estimationWorkItems };
  }

  function handleEstimationsLoadError(err) {
    alert('Error loading estimations');
    console.log('estimationsData.load', err);
    throw err;
  }
};

estimationDetailsDataLoader.$inject = [
  'Promise',
  'spmEstimationsData',
  'spmProjectsData',
  'spmWorkItemsData',
  '$stateParams'
];
