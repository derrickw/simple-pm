export default function detailsFormFields(vm) {
  return [
    {
      type: 'input',
      key: 'name',
      templateOptions: {
        label: 'Name'
      }
    },
    {
      type: 'select',
      key: 'projectId',
      templateOptions: {
        label: 'Project',
        options: vm.projects
      }
    }
  ];
}
