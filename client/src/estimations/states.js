import listDataLoader from './list/list_data_loader';
import detailsDataLoader from './details/details_data_loader';
// import workItemsViewDataLoader from './create '

export default {
  estimations: {
    url: '/estimations',
    abstract: true,
    template: `
      <div ui-view></div>
    `
  },

  list: {
    url: '/list',
    controllerAs: 'vm',
    controller: require('./list/list.ctrl'),
    template: require('./list/list.tmpl'),
    resolve: {
      data: listDataLoader
    }
  },

  details: {
    url: '/:estimationId',
    controllerAs: 'vm',
    controller: require('./details/details.ctrl'),
    template: require('./details/details.tmpl'),
    resolve: {
      data: detailsDataLoader
    }
  },

  create: {
    url: '/create',
    controllerAs: 'vm',
    controller: require('./create/create.ctrl'),
    template: require('./create/create.tmpl')
  },

  newWorkItem: {
    url: '/:estimationId/new-work-item',
    controllerAs: 'vm',
    controller: require('./create/work_item_form.ctrl'),
    template: require('./create/work_item_form.tmpl')
  }
};

export let workItems = {
  workItems: {
    url: '/work-items',
    abstract: true,
    template: `<div ui-view></div>`
  },
  view: {
    url: '/:workItemId',
    controllerAs: 'vm',
    controller: require('./create/work_item_form.ctrl'),
    template: require('./create/work_item_form.tmpl'),
    resolve: {
      data: ['spmWorkItemsData', 'spmEstimationsData', '$stateParams', function(workItemsData, estimationsData, $params) {
        return workItemsData.loadOne($params.workItemId)
          .catch(function (err) {
            debugger
          });
      }]
    }
  },
  form: {}
}
