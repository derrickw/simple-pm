import { bind, clone, findIndex } from 'lodash';

import { workItemFormFields } from './work_item_form_fields';

import {
  getLaborItems,
  getMaterialItems,
  separateItems
} from './work_item_form_helpers';


export default class WorkItemForm {
  constructor($state, newWorkItemEntryModal, costItemsData, workItemsData, data) {
    this._workItemEntryModal = newWorkItemEntryModal;
    this._state = $state;
    this._costItemsData = costItemsData;
    this._workItemsData = workItemsData;

    this.workItemInfo = data;
    this.estimationId = data.estimation;
    this.workItemForm = workItemFormFields;

    this.laborItems = getLaborItems(this.workItemInfo.costItems);
    this.materialItems = getMaterialItems(this.workItemInfo.costItems);
  }

  discard() {
    if (!confirm('Are you sure?')){
      return;
    }

    this._state.go('main.estimations.details', { estimationId: this.estimationId })
  }

  save(workItem) {
    this._workItemsData.store(workItem);
    console.log('saving', workItem);
  }

  saveCostItem(costItem) {
    return this._costItemsData.store(clone(costItem));
  }

  addCostItem(type, costItem) {
    var proc;
    var costItemClone = clone(costItem);

    delete costItemClone.isNew;

    if (costItem.isNew) {
      costItemClone.type = type.toUpperCase();
      proc = this.saveCostItem(costItemClone);
    }
    else {
      proc = Promise.resolve(costItemClone);
    }

    proc.then((baseCostItem) => {
      return {
        costItemBase: baseCostItem,
        rate: baseCostItem.rate,
        quantity: costItemClone.quantity
      };
    }).then((entryItem) => {
      this.workItemInfo.costItems.push(entryItem);

      this.laborItems = getLaborItems(this.workItemInfo.costItems);
      this.materialItems = getMaterialItems(this.workItemInfo.costItems);
    }).then(() => {
      return this.save(this.workItemInfo);
    });
  }
};

WorkItemForm.$inject = [
  '$state',
  'newWorkItemEntryModal',
  'spmCostItemsData',
  'spmWorkItemsData',
  'data'
];
