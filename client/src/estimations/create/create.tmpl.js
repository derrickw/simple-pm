module.exports = `
  <div class="box box-warning container">
    <div class="box-header">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>Project</label>
            <select class="form-control">
              <option ng-repeat="project in vm.projects" value="{{project.id}}">{{project.name}}</option>
            </select>
          </div>
        </div>
        <div class="col-sm-6">
          <button class="btn btn-primary">Add Work Item</button>
          <button class="btn btn-success">Save Estimation</button>
        </div>
      </div>
    </div>
  </div>
`;
