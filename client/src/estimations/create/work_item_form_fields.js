export let workItemFormFields = [
  {
    className: 'row',
    fieldGroup: [
      {
        className: 'col-xs-8',
        type: 'input',
        key: 'name',
        templateOptions: {
          label: 'Name'
        }
      }
      // ,
      // {
      //   className: 'col-xs-2',
      //   type: 'input',
      //   key: 'quantity',
      //   templateOptions: {
      //     label: 'Quantity'
      //   }
      // },
      // {
      //   className: 'col-xs-2',
      //   type: 'input',
      //   key: 'rate',
      //   templateOptions: {
      //     label: 'Rate',
      //     addonLeft: {
      //       text: '$'
      //     }
      //   }
      // }
    ]
  },
  {
    // className: "col-xs-12",
    type: 'textarea',
    key: 'description',
    templateOptions: {
      label: 'Description',
      rows: 3
    }
  },
  {
    template: '<hr />'
  }
]
