module.exports = `<div class="box box-warning container">
    <div class="box-header">
      <div class="box-tools">
        <button class="btn btn-danger" ng-click="vm.discard()">Discard</button>
        <button class="btn btn-success" ng-click="vm.save(vm.workItemInfo)">Save</button>
      </div>
    </div>
    <div class="box-body">

      <form role="form">
        <formly-form fields="vm.workItemForm" model="vm.workItemInfo"></formly-form>
      </form>

      <div class="row">
        <div class="col-sm-6">

          <cost-item-entries-table
            entries="vm.laborItems"
            entry-type="labor"
            entry-added="vm.addCostItem(type, costItem)">
          </cost-item-entries-table>
          
        </div>
        <div class="col-sm-6">

          <cost-item-entries-table
            entries="vm.materialItems"
            entry-type="material"
            entry-added="vm.addCostItem(type, costItem)">
          </cost-item-entries-table>

        </div>
      </div>
    </div>
  </div>`;
