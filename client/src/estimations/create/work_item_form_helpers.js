import { chain, filter, map, omit, extend  } from 'lodash';

export function getLaborItems(allItems) {
  return separateItems(allItems, 'LABOR');
}

export function getMaterialItems(allItems) {
  return separateItems(allItems, 'MATERIAL');
}

export function separateItems(allItems, type) {
  var laborItems = chain(allItems)
    .filter((item) => item.costItemBase.type.toUpperCase() === type)
    .map((item) => {
      var entry = omit(item, 'costItemBase');
      var base = item.costItemBase;

      var newItem = extend({}, base, entry);
      newItem.rate = entry.rate || base.rate;

      return newItem;
    }).value()

  return laborItems;
}
