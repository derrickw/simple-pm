module.exports = EstimationsCreateController

EstimationsCreateController.$inject = [
  '$scope',
  '$modal',
  'spmLocalData'
];

function EstimationsCreateController($scope, $modal, localData) {
  let vm = this;

  vm.projects = [
    {
      name: 'soemting'
    },
    {
      name: 'someting else'
    }
  ]

  localData.get(['laborItems', 'materialItems', 'projects'])
    .then(function(results) {
      vm.laborItems = results[0];
      vm.materialItems = results[1];
    });
};
