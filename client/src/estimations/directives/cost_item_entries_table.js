import { clone, bind } from 'lodash';

let costItemEntriesTemplate = `
  <div class="box">
      <div class="box-header">
        <h3 class="box-title">Labor</h3>
        <div class="box-tools">
          <button href="#" class="btn btn-xs pull-right" ng-click="vm.addEntry()">Add</button>
        </div>
      </div>
      <div class="box-body no-padding">
        <table class="table table-striped table-condensed">
              <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Man Hours</th>
                  <th>Hourly Rate</th>
                  <th>Total</th>
              </tr>
              <tr ng-repeat="labor in vm.entries">
                  <td>{{ $index + 1 }}.</td>
                  <td>{{ labor.name }}</td>
                  <td>{{ labor.quantity }}</td>
                  <td>{{ labor.rate }}</td>
                  <td>{{ labor.total }}</td>
              </tr>
          </table>
      </div>
  </div>`;

export default costItemEntriesTable;

costItemEntriesTable.$inject = [];

function costItemEntriesTable() {
  return {
    restrict: 'AE',
    template: costItemEntriesTemplate,
    scope: {
      entries: '=',
      _entryType: '@entryType',
      entryAdded: '&'
    },
    bindToController: true,
    controllerAs: 'vm',
    controller: CostItemEntriesTableController
  }
}

class CostItemEntriesTableController {
  constructor(itemEntryModal) {
    this._itemEntryModal = itemEntryModal
  }

  addEntry() {
    this._itemEntryModal.show({ type: this._entryType })
      .then(bind(doEntryAdded, this));
  }
}

function doEntryAdded(costItem) {
  this.entryAdded({ type: this._entryType, costItem: costItem });
}

CostItemEntriesTableController.$inject = ['newWorkItemEntryModal'];
