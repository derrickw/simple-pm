import template from './work_item_entry_form.tmpl';

let app = angular.module('simple-pm');

export default function WorkItemEntryForm() {

  return {
    restrict: 'EA',
    template: template,
    link: link,
    controllerAs: 'vm',
    bindToController: true,
    scope: {
      mode: '@',
      type: '@',
      ngModel: '='
    },
    controller: WorkItemEntryFormController
  };

  function link(scope, el, attrs) {

  }

}

class WorkItemEntryFormController {
  constructor() {
    this.rememberNewTargetWorkItem = true;
  }

  get rememberNewTargetWorkItem() {
    return this._rememberNewTargetWorkItem;
  }

  set rememberNewTargetWorkItem(value) {
    this._rememberNewTargetWorkItem = value;
  }
}
