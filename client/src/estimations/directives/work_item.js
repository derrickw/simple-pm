import formTemplate from './work_item.tmpl';
import readOnlyTemplate from './work_item_read_only.tmpl';

export default function workItem($compile) {

  return {
    restrict: 'E',
    scope: {
      workItem: '=item'
    },
    controllerAs: 'vm',
    bindToController: true,
    controller: WorkItemDirectiveController,
    link: (scope, el, attrs) => {
      let tmpl = ('readonly' in attrs) ? readOnlyTemplate : formTemplate;
      el.html(tmpl).show();
      $compile(el.contents())(scope);
    }
  };

  function WorkItemDirectiveController() {
    let vm = this;
    vm.workItemFields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name'
        }
      }
    ];
  }
}

workItem.$inject = ['$compile'];
