export default `
<div class="work-item-entry-form">
  <div class="row">

    <div class="col-xs-7">
      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" />
      </div>
    </div>

    <div class="col-xs-2">
      <div class="form-group">
        <label>Hours</label>
        <input class="form-control" placeholder="" ng-model="vm.targetWorkItem.quantity" />
      </div>
    </div>

    <div class="col-xs-3">
      <div class="form-group">
        <label>Rate</label>
        <div class="input-group">
          <span class="input-group-addon">$</span>
          <input type="text" class="form-control" placeholder="" ng-model="vm.targetWorkItem.cost" />
        </div>
      </div>
    </div>

  </div>

  <div class="row">
    <div class="col-xs-12">
      <div class="form-group">
        <input type="checkbox" ng-model="vm.rememberNewTargetWorkItem"><label>Remember this work item?</label>
      </div>
    </div>
  </div>
</div>`;
