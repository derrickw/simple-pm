import angular from 'angular';
import workItem from './work_item';
import workItemEntryForm from './work_item_entry_form';
import costItemEntriesTable from './cost_item_entries_table';

let app = angular.module('simple-pm');

app.directive('workItem', workItem);
app.directive('workItemEntryForm', workItemEntryForm);
app.directive('costItemEntriesTable', costItemEntriesTable);
