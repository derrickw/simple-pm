export default `
  <div class="work-item-read-only box box-primary pull-left" style="width: 31%; margin-right: 2%">
    <div class="box-header">
      <a ui-sref="main.workItems.view({ workItemId: vm.workItem._id })">
        <h3 class="box-title"><strong>Name:</strong> {{vm.workItem.name}}<h3>
      </a>
    </div>
    <div class="box-body"></div>
  </div>
`;
