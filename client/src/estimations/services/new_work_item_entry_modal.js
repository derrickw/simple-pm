import template from './new_work_item_entry_modal.tmpl.js';
import Controller from './new_work_item_entry_modal.ctrl.js';

export default newWorkItemEntryModal;

newWorkItemEntryModal.$inject = [
  '$modal',
  'spmCostItemsData'
];

function newWorkItemEntryModal($modal, costItemsData) {
  return {
    show
  };

  function show(options = {}) {
    if (!options.type) {
      alert('Cannot show modal. Check console.');
      console.error('Missing type option for new work item entry modal');
      return;
    }

    let modalInstance = $modal.open({
      template: template,
      controllerAs: 'vm',
      controller: Controller,
      resolve: {
        costType: function () {
          return options.type;
        },
        costItems: function() {
          return costItemsData.load({ type: options.type.toUpperCase() });
        }
      }
    });

    return modalInstance.result
      .then(function(results) {
        console.log('adding work item', results);
        return results;
      })
      .catch(function(err) {
        console.log('modal instance error', err);
        throw err;
      });
  }
}
