import { clone } from 'lodash';

import costItemEntryFormFields from './cost_item_entry_form_fields';

export default class NewWorkItemEntryModalController {
  constructor($modalInstance, costType, costItems) {
    this._chooseExising = false;
    this._modal = $modalInstance;

    this._rememberNewTargetWorkItem = true;
    this._itemAddOption = null;

    this.type = costType;
    this.costItemEntryInfo = {};
    this.availableWorkItems = addNewOption(costItems);
    this.costItemEntryForm = costItemEntryFormFields(this);
  }

  get itemAddOption() {
    return this._itemAddOption;
  }

  get costTypeDisplay() {
    return this.type[0].toUpperCase() + this.type.slice(1);
  }

  set itemAddOption(value) {
    this._itemAddOption = value;
    this.costItemEntryInfo = createEntryItem(value);
  }

  save(workItem) {
    this._modal.close(this.costItemEntryInfo);
  }

  close() {
    if (!confirm('Are you sure you want to close? Any changes you\'ve made will be lost.')) {
      return;
    }

    this._modal.dismiss();
  }
}

NewWorkItemEntryModalController.$inject = [
  '$modalInstance',
  'costType',
  'costItems'
];

function addNewOption(items) {
  items.unshift({
    isNew: true,
    name: 'Add new item ...'
  });

  return items;
}

function createEntryItem(value) {
  var item = clone(value);
  item.name = value.isNew ? '' : item.name;
  return item;
}
