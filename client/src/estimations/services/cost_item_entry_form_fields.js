export default function costItemEntryFormFields(vm) {
  return [
    {
      className: 'row',
      fieldGroup: [
        {
          className: 'col-xs-6',
          type: 'input',
          key: 'name',
          templateOptions: {
            label: 'Name'
          },
          expressionProperties: {
            'templateOptions.disabled': '!model.isNew'
          }
        },
        {
          className: 'col-xs-2',
          type: 'input',
          key: 'quantity',
          templateOptions: {
            label: 'Quantity'
          }
        },
        {
          className: 'col-xs-4',
          type: 'input',
          key: 'rate',
          templateOptions: {
            label: 'Rate',
            addonLeft: {
              text: '$'
            }
          },
          expressionProperties: {
            'templateOptions.disabled': '!model.isNew'
          }
        }
      ]
    }
  ];
};
