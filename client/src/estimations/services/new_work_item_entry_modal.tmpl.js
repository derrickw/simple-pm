export default `
  <div class="modal-header">
      <h3 class="modal-title">Add {{vm.costTypeDisplay}} Cost</h3>
  </div>
  <div class="modal-body">
      <div class="row new-work-item-select">
        <div class="col-xs-12">
          <h4>Choose</h4>
          <select class="form-control"
            ng-options="::workItem.name for workItem in ::vm.availableWorkItems"
            ng-model="vm.itemAddOption">
          </select>
        </div>
      </div>

      <div class="entry-form" ng-show="vm.itemAddOption">
        <form>
          <formly-form fields="vm.costItemEntryForm" model="vm.costItemEntryInfo"></formly-form>
        </form>
      </div>

  </div>
  <div class="modal-footer">
      <button class="btn btn-primary" ng-click="vm.save(vm.costItemEntryInfo)">Save</button>
      <button class="btn btn-warning" ng-click="vm.close()">Cancel</button>
  </div>
`;
