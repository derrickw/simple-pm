import newWorkItemEntryModal from './new_work_item_entry_modal';

let app = require('angular').module('simple-pm');

app.factory('newWorkItemEntryModal', newWorkItemEntryModal);

app.factory('newEstimationModal', function($modal, _) {

  return {
    show: showModal
  };

  function showModal() {
    let instance = $modal.open({
      template: `
        <div class="modal-body">
          <form>
            <formly-form model="vm.estimationInfo" fields="vm.fields">
              <button ng-click="vm.save()">Save</button>
            </formly-form>
          </form>
        </div>
      `,
      controllerAs: 'vm',
      controller: ['$modalInstance', 'spmProjectsData', '$filter', function($modalInstance, projects, $filter) {
        var vm = this;

        projects.load()
          .then(function(results) {
            vm.fields = getFields({ projects: formatProjects(results) });
          });

        vm.estimationInfo = {
          name: 'New Estimation ' + $filter('date')((new Date()).getTime(), 'MM/dd/yyyy' ),
          description: '[description]'
        };
        // vm.fields = getFields();

        vm.save = save;

        function save() {
          console.log('modal', $modalInstance, instance);
          instance.close(vm.estimationInfo);
        }
      }]
    });

    return instance.result;

    function formatProjects(raw) {
      return _.map(raw, function(project) {
        return {
          name: project.name,
          value: project._id
        };
      });
    }

    function getFields(options) {
      return [
          {
            type: 'input',
            key: 'name',
            templateOptions: {
              label: 'Label'
            }
          },
          {
            type: 'select',
            key: 'project',
            templateOptions: {
              label: 'Project',
              options: options.projects
            }
          },
          {
            type: 'textarea',
            key: 'description',
            templateOptions: {
              label: 'Description'
            }
          }
        ]
    }
  }

});
