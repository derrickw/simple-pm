module.exports = ProjectsListController;

ProjectsListController.$inject = ['$scope', '$filter', 'newProjectModal', 'spmProjectsData', '$state', '$http'];

function ProjectsListController($scope, $filter, newProjectModal, projectsData, $state, $http) {
  var vm = this;

  vm.showNewProjectForm = showNewProjectForm;

  projectsData.load()
    .then(function(projects) {
      vm.projects = projects;
    });

  function showNewProjectForm() {
    newProjectModal.show()
      .then(function(projectInfo) {
        return $http.post('/api/projects', { name: projectInfo.name });
      })
      .then(function(results) {
        return projectsData.store(results.data[0]);
      })
      .then(function(results) {
        results.id = results._id;
        return results;
      })
      .then(function(results) {
        $state.go('main.projects.details', { projectId: results.id });
      });
  }

  function formatData(data) {
    var formatted = [];

    angular.forEach(data, function(item) {
      item.name = item.name.substring(0, 15, '...');
      item.created = $filter('date')(item.created, 'mediumDate');
      formatted.push(item);
    });

    return formatted;
  }

  vm.doButtonAction = function doButtonAction(actionName, ...params) {
    var actions = {
      view() {
        alert('view');
        console.log('view', arguments);
      },
      edit(projectId) {
        alert('edit');
        console.log('edit', arguments);
      },
      addEstimation(projectId) {
        alert('addEstimation');
        console.log('addEstimation', arguments);
      }
    };

    var targetAction = actions[actionName];
    if (!targetAction) {
      alert('Invalid action: ' + actionName);
      return;
    }

    targetAction.apply(null, params);
  }
}
