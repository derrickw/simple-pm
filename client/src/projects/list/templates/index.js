module.exports = `
  <div class="box box-primary">
      <div class="box-header">
          <h3 class="box-title">Projects</h3>
          <div class="pull-right box-tools">
              <button class="btn btn-default btn-sm pull-right" ng-click="vm.showNewProjectForm()"><i class="fa fa-plus"></i></button>
          </div>

      </div>
      <div class="box-body">

        <div class="clearfix">
          <div class="box box-primary pull-left" ng-repeat="project in vm.projects" style="width: 31%; margin-right: 2%">
            <div class="box-header">
                <h3 class="box-title"><a ui-sref="main.projects.details({ projectId: project._id })">{{ project.name }}</a></h3>
                <div class="box-tools pull-right">
                  <div class="btn-group">
                      <!-- <button type="button" class="btn btn-xs btn-default"><i class="fa fa-ellipsis-h"></i></button> -->
                      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-gear"></i>
                          <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="pull-right dropdown-menu" role="menu">
                          <li><a href="#" class="" ng-click="vm.doButtonAction('edit', project.id)">Edit</a>
                          <li><a href="#" class="" ng-click="vm.doButtonAction('addEstimation', project.id)">Add Estimation</a></li>
                          <li><a ui-sref="main.projects.details({ projectId: project._id })"
                              class="">View Project</a></li>
                          <li><a href="#">Hide</a></li>
                      </ul>
                  </div>
                </div>
            </div>
            <div class="box-body">
                <!-- <p>{{ project.created }}</p> -->
                <!-- Box class: <code>.box</code>
                <p>
                    amber, microbrewery abbey hydrometer, brewpub ale lauter tun saccharification oxidized barrel.
                    berliner weisse wort chiller adjunct hydrometer alcohol aau!
                    sour/acidic sour/acidic chocolate malt ipa ipa hydrometer.
                </p> -->
            </div><!-- /.box-body -->
            <!-- <div class="box-footer">
              <a href="#" class="btn btn-sm btn-default" ng-click="vm.doButtonAction('edit', project.id)">Edit</a>
              <a href="#" class="btn btn-sm btn-default" ng-click="vm.doButtonAction('addEstimation', project.id)">Add Estimation</a>
              <a ui-sref="main.projects.details({ projectId: project.id })"
                  class="btn btn-sm btn-default">View Project</a>
              <a href="#" class="btn btn-sm btn-default">Hide</a>
              <div class="btn-group">

                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-ellipsis-h"></i>
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="pull-right dropdown-menu" role="menu">
                      <li><a href="#" class="" ng-click="vm.doButtonAction('edit', project.id)">Edit</a>
                      <li><a href="#" class="" ng-click="vm.doButtonAction('addEstimation', project.id)">Add Estimation</a></li>
                      <li><a ui-sref="main.projects.details({ projectId: project.id })"
                          class="">View Project</a></li>
                      <li><a href="#">Hide</a></li>
                  </ul>
              </div>
            </div> -->
        </div><!-- /.box -->
      </div>

          <!-- <table class="table table-bordered table-condensed table-hover">
              <tr>
                  <th style="width: 10px">#</th>
                  <th>Task</th>
                  <th>Created</th>
                  <th style="width: 100px"></th>
              </tr>
              <tr ng-repeat="project in vm.projects">
                  <td>{{ project.id }}</td>
                  <td>{{ project.name }}</td>
                  <td>{{ project.created }}</td>
                  <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-h"></i>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="pull-right dropdown-menu" role="menu">
                            <li><a href="#" class="" ng-click="vm.doButtonAction('edit', project.id)">Edit</a>
                            <li><a href="#" class="" ng-click="vm.doButtonAction('addEstimation', project.id)">Add Estimation</a></li>
                            <li><a ui-sref="main.projects.details({ projectId: project.id })"
                                class="">View Project</a></li>
                            <li><a href="#">Hide</a></li>
                        </ul>
                    </div>
                  </td>
              </tr>
          </table> -->
      </div>
      <div class="box-footer clearfix">

      </div>
  </div>
  `
