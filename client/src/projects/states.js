module.exports = {
  projects: {
    url: '/projects',
    abstract: true,
    template: `<div ui-view></div>`
  },

  details: {
    url: '/:projectId',
    controllerAs: 'vm',
    controller: require('./details/controllers'),
    template: require('./details/templates')
  },

  list: {
    url: '/list',
    controllerAs: 'vm',
    controller: require('./list/controllers/'),
    template: require('./list/templates')
  }
};
