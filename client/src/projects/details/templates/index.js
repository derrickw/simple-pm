module.exports = `
  <form novalidate ng-submit="vm.saveProject()">
    <a ui-sref="main.projects.list">back to project list</a>
    <formly-form model="vm.project" fields="vm.projectFields"></formly-form>
    <button class="btn btn-primary">Save</button>
  </form>
`;
