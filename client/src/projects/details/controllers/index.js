module.exports = ProjectDetailsController;

ProjectDetailsController.$inject = [
  '$stateParams',
  'spmProjectsData',
  '$state'
];

function ProjectDetailsController($stateParams, projectsData, $state) {
  var vm = this;

  projectsData.loadOne($stateParams.projectId)
    .then((project) => {
      vm.project = project;
    })
    .catch((err) => {
      alert('Error loading project');
      $state.go('main.projects.list');
      console.log(err);
    });

  vm.saveProject = saveProject;

  function saveProject(){
    projectsData
      .store(vm.project)
      .then(function(results) {
        console.log('results', results);
        alert('saved');
      })
      .catch(function(err) {
        console.log('store error', err);
      })
  }

  vm.projectFields = [
    {
      type: 'input',
      key: 'name',
      templateOptions: {
        label: 'Name'
      }
    }
  ];
}
