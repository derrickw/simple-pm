newProjectModal.$inject = ['$modal'];

module.exports = newProjectModal;

function newProjectModal($modal) {

  return {
    show: showModal
  };

  function showModal() {
    let instance = $modal.open({
      template: `
        <div class="modal-body">
          <form>
            <formly-form model="vm.projectInfo" fields="vm.fields">
              <button ng-click="vm.save()">Save</button>
            </formly-form>
          </form>
        </div>
      `,
      controllerAs: 'vm',
      controller: ['$modalInstance', function newProjectModalController($modalInstance) {
        var vm = this;

        vm.projectInfo = {};
        vm.fields = getFields();

        vm.save = function save() {
          instance.close(vm.projectInfo);
        };
      }]
    });

    return instance.result;

    function getFields() {
      return [
        {
          type: 'input',
          key: 'name',
          templateOptions: {
            label: 'Name'
          }
        }
      ]
    }
  }

}
