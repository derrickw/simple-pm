module.exports = {
  reports: {
    url: '/reports',
    abstract: true,
    template: `<div ui-view></div>`
  },

  view: {
    url: '/:reportId',
    controllerAs: 'vm',
    controller: require('./view/controllers'),
    template: require('./view/templates')
  },

  list: {
    url: '/list',
    controllerAs: 'vm',
    controller: require('./list/controllers/'),
    template: require('./list/templates')
  }
};
