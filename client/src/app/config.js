import estimationsStates, { workItems } from '../estimations/states';

// let estimationsStates = require('../estimations/states');
let projectsStates = require('../projects/states');
let reportsStates = require('../reports/states');

appConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

export default function appConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('main', {
      url: '',
      abstract: true,
      controllerAs: 'mainVm',
      template: `<div class="main-view" ui-view><h1>main</h1></div>`,
      controller: require('./controllers/spm_main_controller')
    })
    .state('main.dashboard', require('../dashboard/states'))

    .state('main.projects', projectsStates.projects)
    .state('main.projects.list', projectsStates.list)
    .state('main.projects.details', projectsStates.details)

    .state('main.estimations', estimationsStates.estimations)
    .state('main.estimations.list', estimationsStates.list)
    .state('main.estimations.details', estimationsStates.details)
    .state('main.estimations.create', estimationsStates.create)

    .state('main.workItems', workItems.workItems)
    .state('main.workItems.view', workItems.view)
    .state('main.workItems.form', workItems.form)
    .state('main.workItems.create', estimationsStates.newWorkItem)

    .state('main.reports', reportsStates.reports)
    .state('main.reports.list', reportsStates.list)
    .state('main.reports.view', reportsStates.view);

  $urlRouterProvider.otherwise('/dashboard');
};
