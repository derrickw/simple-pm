module.exports = appSetup

function appSetup(app) {
  app.controller('SpmMainController', require('./controllers/spm_main_controller'));

  app.directive('spmSidebarMenu', require('./directives/sidebar_menu'));
  app.directive('spmTopNav', require('./directives/top_nav'));
}
