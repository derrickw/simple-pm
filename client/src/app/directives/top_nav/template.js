module.exports = `
  <header class="header">

      <a href="index.html" class="logo">
          <strong>MJC</strong>Southside
      </a>

      <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-right">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="glyphicon glyphicon-th"></i>
                          <!-- <span>Jane Doe <i class="caret"></i></span> -->
                      </a>
                      <ul class="dropdown-menu">

                          <!-- <li class="user-header bg-light-blue">
                              <img src="/assets/adminlte/img/avatar3.png" class="img-circle" alt="User Image" />
                              <p>
                                  Jane Doe - Web Developer
                                  <small>Member since Nov. 2012</small>
                              </p>
                          </li> -->

                          <li class="user-body">
                              <div class="col-xs-4 text-center">
                                <a ui-sref="main.projects.list" ng-click="vm.setActive('projects')">
                                  <i class="fa fa-folder-o"></i> <span>Projects</span>
                                </a>
                              </div>
                              <div class="col-xs-4 text-center">
                                <a ui-sref="main.estimations.list" ng-click="vm.setActive('estimations')">
                                  <i class="fa fa-clock-o"></i> <span>Estimations</span>
                                </a>
                              </div>
                              <div class="col-xs-4 text-center">
                                <a ui-sref="main.reports.list" ng-click="vm.setActive('reports')">
                                  <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
                                </a>
                              </div>
                          </li>

                          <li class="user-footer">
                              <div class="pull-right">
                                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                              </div>
                          </li>
                      </ul>
                  </li>
              </ul>
          </div>
      </nav>
  </header>
`
