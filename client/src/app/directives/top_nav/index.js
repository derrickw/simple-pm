let angular = require('angular');

module.exports = function SpmTopNav() {
  return {
    restrict: 'AE',
    scope: {},
    replace: true,
    template: require('./template'),
    controllerAs: 'vm',
    controller: require('./controller.js'),
    link: function SpmTopNavLink(scope, el, attr) {
      angular.element(el).click(function(e) {
          e.preventDefault();
          //If window is small enough, enable sidebar push menu
          if ($(window).width() <= 992) {
              $('.row-offcanvas').toggleClass('active');
              $('.left-side').removeClass("collapse-left");
              $(".right-side").removeClass("strech");
              $('.row-offcanvas').toggleClass("relative");
          } else {
              //Else, enable content streching
              $('.left-side').toggleClass("collapse-left");
              $(".right-side").toggleClass("strech");
          }
      });
    }
  };
};
