var angular = require('angular');
var app = angular.module('simple-pm');

app.directive('spmSidebarMenu', require('./sidebar_menu'));
app.directive('spmTopNav', require('./top_nav'));