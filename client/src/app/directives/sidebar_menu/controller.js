module.exports = function SpmSidebarMenuController() {
  var vm = this;
  var activeSection = 'dashboard';

  vm.isActive = function sectionIsActive(section) {
    return activeSection === section;
  };

  vm.setActive = function setActiveSection(section, $event) {
    activeSection = section;
  };
};
