module.exports = function SpmSideBarMenu() {
  return {
    restrict: 'AE',
    scope: {},
    template: require('./template'),
    controllerAs: 'vm',
    controller: require('./controller.js')
  };
};
