module.exports = `
  <section class="sidebar">
    <ul class="sidebar-menu" ng-click="vm.setActiveItem()">
      <li class="active" ng-class="{ active: vm.isActive('dashboard') }">
        <a ui-sref="main.dashboard" ng-click="vm.setActive('dashboard')">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li ng-class="{ active: vm.isActive('projects') }">
        <a ui-sref="main.projects.list" ng-click="vm.setActive('projects')">
          <i class="fa fa-folder-o"></i> <span>Projects</span>
        </a>
      </li>
      <li ng-class="{ active: vm.isActive('estimations') }">
        <a ui-sref="main.estimations.list" ng-click="vm.setActive('estimations')">
          <i class="fa fa-clock-o"></i> <span>Estimations</span>
        </a>
      </li>
      <li ng-class="{ active: vm.isActive('estimations') }">
        <a ui-sref="main.reports.list" ng-click="vm.setActive('reports')">
          <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
        </a>
      </li>
    </ul>
  </section>
`;
