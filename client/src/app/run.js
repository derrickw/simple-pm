module.exports = spmRun;

spmRun.$inject = ["$rootScope", 'Promise'];

function spmRun($rootScope, Promise) {
    // syncs the angular digest cycle and the Promise timing
    Promise.setScheduler(function (cb) {
        $rootScope.$evalAsync(cb);
    });
}
