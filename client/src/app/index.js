var angular = require('angular');
var app = angular.module('simple-pm');

app.value('Promise', require('bluebird'));
app.value('_', require('lodash'));

app.run(require('./run'));
app.config(require('./config'));

require('./directives');
require('../services');

require('../estimations');
require('../projects');
