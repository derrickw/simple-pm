let angular = require('angular');
let moduleDeps;

// loading non-module scripts
// var router = require('ui.router');
// require('ui.bootstrap.modal');
// require('ui.bootstrap.tpls');

require('angular-ui-router');
require('angular-bootstrap');
;

moduleDeps = [
  'ui.router',
  'ui.bootstrap.modal',
  'ui.bootstrap.tpls',
  require('./services').name,
  require('simplepm-angular').name,
  require('angular-formly'),
  require('angular-formly-templates-bootstrap')
];

let app = module.exports = angular.module('simple-pm', moduleDeps);
trackDigests(app);

function trackDigests(app) {
  app.run(["$rootScope", 'Promise', function ($rootScope, Promise) {
    Promise.setScheduler(function (cb) {
      $rootScope.$evalAsync(cb);
    });
  }]);
}

require('./app/index');
