var _ = require('lodash');

module.exports = {
  laborRateCalculator: function laborRateCalculator(rate, qty, options) {
    options = options || {};
    return rate * qty;
  }
};
