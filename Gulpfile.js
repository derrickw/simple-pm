'use strict';

var gulp = require('gulp'),
    babel = require('gulp-babel'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    watchify = require('watchify'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    stylus = require('gulp-stylus');

gulp.task('watch', ['watch:js', 'watch:stylus']);

gulp.task('watch:js', ['build'], function(){
  return gulp.watch([
    './client/src/**/*.js',
    './lib/**/*.js',
    './lib-angular/**/*.js',
    './Gulpfile.js'
  ], ['build']);
});

gulp.task('watch:stylus', ['build:stylus'], function() {
  return gulp.watch('./client/assets/**/*.styl', ['build:stylus']);
});

gulp.task('build', function() {

  return browserify({
    entries: './client/src/index.js',
    exclude: [
      // 'angular',
      // 'ui.router',
      // 'ui.bootstrap.modal',
      // 'ui.bootstrap.tpls',
      // 'angular-formly',
      // 'angular-formly-templates-bootstrap'
    ],
    debug: false
  })
  .transform(babelify.configure({
    only: /client\/src/
  }))
  .bundle()
  .pipe(source('build.js'))
  .pipe(gulp.dest('./client/assets'));

});

gulp.task('build:stylus', function() {
  return gulp.src('./client/assets/**/*.styl')
    .pipe(stylus())
    .pipe(concat('simple-pm.css'))
    .pipe(gulp.dest('./client/assets/'))
})
