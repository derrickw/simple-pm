'use strict';

var express = require('express'),
  join = require('path').join,
  app = express();

app.set('view engine', 'ejs');
app.set('views', join(__dirname, './views'));
app.use('/assets', express.static('./client/assets'));

app.get('/ui', function(req, res) {
  res.render('admin');
});
app.get('/ui/empty', function(req, res) {
  res.render('empty');
});

app.use('/api', require('./api'));

app.listen(7654, function() {
  console.log('listening', 7654);
});
