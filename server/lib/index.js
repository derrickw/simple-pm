var _ = require('lodash');

module.exports = {
  createLogger: function createLogger(options) {
    options = _.merge({
      name: 'Unnamed Logger',
      correlationId: null,
      extras: []
    }, options);

    var log = _.bind(console.log, console);
    var logError = _.bind(console.error, console);
    var logInfo = _.bind(console.info, console);
    var logWarn = _.bind(console.warn, console);

    return {
      log: _.partial(log, options.name),
      error: _.partial(logError, options.name),
      info: _.partial(logInfo, options.name),
      warn: _.partial(logWarn, options.name)
    };
  }
};
