var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));

var dbConnection = null;

module.exports = function() {
  if (dbConnection) {
    return Promise.resolve(dbConnection);
  }

  return mongoose.connectAsync('mongodb://localhost:27017/SIMPLEPM')
    .then(function() {
      dbConnection = mongoose.connection;
    })
    .catch(function(err) {
      console.log('connect error', err);
      return mongoose.connection;
    });
};
