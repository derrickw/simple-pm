console.log('bubbles');

var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));

var projectMongoose = require('./mongoose');
var Project = Promise.promisifyAll(projectMongoose.Model);

var dbConnect = require('../mongoose_connect');

module.exports = {
  find: function find(query) {
    query = query || {};

    return Promise.try(dbConnect)
      .then(function() {
        return new Promise(function(resolve, reject) {
          Project.find(query, function(err, results) {
            return err ? reject(err) : resolve(results);
          });
        });
      })
      .catch(function(err) {
        throw err;
      });
  },
  save: function save(data) {
    var promise = Promise.try(dbConnect);
    var doc;

    if (data._id) {
      promise = promise.then(function() {
        doc = _.omit(data, ['_id']);
        return Project.updateAsync({ _id: mongoose.Types.ObjectId(data._id) }, doc, {});
      });
    }
    else {
      doc = new Project(data);
      promise = promise.then(function() {
        return doc.saveAsync();
      });
    }

    return promise;
  },
  deactivate: function deactivate(id) {
    // for now, I'm only changing the status
    // I'll worry about removing data later
    var promise = Promise.try(dbConnect);

    return promise
      .then(function() {
        return Project.findOneAsync({ _id: mongoose.Types.ObjectId(id) });
      })
      .then(function(found) {
        found.status = 'INACTIVE';
        return found.saveAsync();
      });
  }
};
