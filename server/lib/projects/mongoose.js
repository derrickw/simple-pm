var mongoose = require('mongoose');

var Schema;
var Model;

Schema = {
  name: { type: String, required: true },
  status: { type: String, enum: ['ACTIVE', 'INACTIVE', 'DELETED'], default: 'ACTIVE' },
  created: { type: Date, default: Date.now }
};

Model = mongoose.model('Project', Schema);

module.exports = {
  Schema: Schema,
  Model: Model
};
