var mongoose = require('mongoose');

var Schema;
var Model;
var mongooseService;

// make sure models are defined
require('../projects')

Schema = {
  name: { type: String, required: true },
  status: { type: String, enum: ['ACTIVE', 'INACTIVE', 'DELETED'], default: 'ACTIVE' },
  created: { type: Date, default: Date.now },
  project: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Project' },
  workItems: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'WorkItem'
  }]
};

Model = mongoose.model('Estimation', Schema);

module.exports = {
  Schema: Schema,
  Model: Model
};
