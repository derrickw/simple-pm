var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));

var estimationsMongoose = require('./mongoose');
var Estimation = Promise.promisifyAll(estimationsMongoose.Model);

var dbConnect = require('../mongoose_connect');

var dbConnection = null;

var estimations = module.exports = {
  findOne: function findOne(id) {
    return Promise.try(dbConnect)
      .then(function() {
        return new Promise(function(resolve, reject) {
          Estimation.findById(id, {}, {}, function(err, estimation) {
            return err ? reject(err) : resolve(estimation)
          });
        });
      })
  },

  find: function find(query, projection) {
    return Promise.try(dbConnect)
      .then(function() {
        return new Promise(function(resolve, reject) {
          Estimation.find({}).populate('project').exec(function(err, results) {
            return err ? reject(err) : resolve(results);
          });
        })
      })
      .catch(function(err) {
        throw err;
      });
  },
  save: function save(data) {
    var promise = Promise.try(dbConnect);
    var doc;

    if (data._id) {
      promise = promise.then(function() {
        doc = _.omit(data, ['_id']);
        return Estimation.updateAsync({ _id: mongoose.Types.ObjectId(data._id) }, doc, {})
      });
    }
    else {
      doc = new Estimation(data);
      promise = promise.then(function() {
        return doc.saveAsync();
      })
    }

    return promise;
  },
  deactivate: function deactivate(id) {
    // for now, I'm only changing the status
    // I'll worry about removing data later
    var promise = Promise.try(dbConnect);

    return promise
      .then(function() {
        return Estimation.findOneAsync({ _id: mongoose.Types.ObjectId(id) });
      })
      .then(function(found) {
        found.status = 'INACTIVE';
        return found.saveAsync();
      });
  }
};
