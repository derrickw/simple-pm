var mongoose = require('mongoose');

var Schema;
var Model;

require('../cost_items');

Schema = {
  name: { type: String, required: true },
  description: { type: String },
  status: { type: String, enum: ['ACTIVE', 'INACTIVE', 'DELETED'], default: 'ACTIVE' },
  created: { type: Date, default: Date.now },
  estimation: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Estimation'
  },
  costItems: [{
    costItemBase: { type: mongoose.Schema.Types.ObjectId, ref: 'CostItem', required: true},
    quantity: { type: Number, required: true },
    rate: { type: Number, default: null }
  }]
};

Model = mongoose.model('WorkItem', Schema);

module.exports = {
  Schema: Schema,
  Model: Model
};
