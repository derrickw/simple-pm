var _ = require('lodash');
var partial = _.partial;
var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));

var workItemsMongoose = require('./mongoose');
var WorkItem = Promise.promisifyAll(workItemsMongoose.Model);

var dbConnect = require('../mongoose_connect');

module.exports = {

  findOne: function findOne(id) {
    return Promise.try(dbConnect)
      .then(function() {
        return WorkItem.findById(id).populate('costItems.costItemBase project').execAsync();
      })
  },

  find: function find(query) {
    return Promise.try(dbConnect)
      .then(function() {
        return WorkItem.find(query).populate('costItems.costItemBase project').execAsync();
      })
      .catch(function(err) {
        console.log('workItems.find', err);
        throw err;
      });
  },

  save: function save(data) {
    var promise = Promise.try(dbConnect);
    var doc;

    console.log('inside save', data);

    if (data._id) {
      promise = promise.then(function() {
        var doc = _.omit(data, ['_id']);
        /*
            { $set { costItems: doc.costitems} }
        */
        console.log('saving doc', doc);

        return WorkItem.updateAsync({ _id: mongoose.Types.ObjectId(data._id) }, doc)
      })
      .catch(function(err) {
        console.log('err', err);
        throw err;
      });
    }
    else {
      doc = new WorkItem(data);
      promise = promise.then(function() {
        return doc.saveAsync();
      });
    }

    return promise;

    function prepForUpdate(dataIn) {
      return dataIn;
    }
  },
  deactivate: function deactivate(id) {
    // for now, I'm only changing the status
    // I'll worry about removing data later
    var promise = Promise.try(dbConnect);

    console.log('deactivate', id);

    return promise
      .then(function() {
        return WorkItem.findOneAsync({ _id: mongoose.Types.ObjectId(id) });
      })
      .then(function(found) {
        found.status = 'INACTIVE';
        return found.saveAsync();
      });
  }
};



// var estimations = module.exports = {
//   find: function find(query, projection) {
//     return Promise.try(dbConnect)
//       .then(function() {
//         return WorkItem.findAsync(query, projection);
//       });
//   },
//   save: function save(data) {
//     debugger
//     var doc = data._id ? data : new WorkItem(data);
//
//     return Promise.try(dbConnect)
//       .then(doc.save)
//       .catch(function (err) {
//         console.log('save error', err);
//       });
//   },
//   deactivate: function deactivate(id) {
//     // for now, I'm only changing the status
//     // I'll worry about removing data later
//     Promise.try(dbConnect)
//       .then(function() {
//
//       })
//   }
// };
