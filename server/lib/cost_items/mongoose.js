var mongoose = require('mongoose');

var Schema;
var Model;
var mongooseService;

Schema = {
  name: { type: String, required: true },
  description: { type: String, required: false },
  status: { type: String, enum: ['ACTIVE', 'INACTIVE', 'DELETED'], default: 'ACTIVE' },
  created: { type: Date, default: Date.now },
  type: { type: String, required: true, enum: ['LABOR', 'MATERIAL']},
  rate: { type: Number, required: true, default: null }
};

Model = mongoose.model('CostItem', Schema);

module.exports = {
  Schema: Schema,
  Model: Model
};
