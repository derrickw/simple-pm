var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));
var dbConnect = require('../mongoose_connect');

var costItemsMongoose = require('./mongoose');
var CostItem = Promise.promisifyAll(costItemsMongoose.Model);

module.exports = {
  findOne: findOne,
  find: find,
  save: save
  // deactivate: deactivate
};

function findOne(id) {
  return connect()
    .then(function () {
      return CostItem.findByIdAsync(id);
    })
    .catch(function (err) {
      console.log('cost item find error', err);
      throw err;
    });
}

function find(query) {
  return connect()
    .then(function () {
      return CostItem.find(query);
    })
    .catch(function (err) {
      console.log('cost item find error', err);
      throw err;
    });
}

function save(data) {
  var promise = connect();
  var doc;

  if (data._id) {
    return promise.then(updateDoc);
  }
  else {
    doc = new CostItem(data);
    return promise.then(saveDoc);
  }

  function updateDoc() {
    doc = _.omit(data, ['_id']);
    return CostItem.updateAsync(idQuery(data._id), doc, {});
  }

  function saveDoc() {
    doc = new CostItem(data);
    return doc.saveAsync();
  }
}


function connect() {
  return Promise.try(dbConnect);
}

function idQuery(id) {
  return { _id: mongoose.Types.ObjectId(id) };
}
