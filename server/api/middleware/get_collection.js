module.exports = getCollection;

function getCollection(req, res, next) {
  try {
    req.collection = require('../../lib/' + req.params.collection);
    next();
  }
  catch (ex) {
    console.log(ex);
    console.log(ex.code);
    res.status(404);
    next(req.params.collection + ' not found.');
  }
}
