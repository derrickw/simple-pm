module.exports = getCollection;

function getCollection(req, res, next) {
  var targetFields = {
    projects: ['name', 'id', '_id'],
    estimations: ['name', 'project', 'id', '_id'],
    work_items: ['name', 'description', 'id', '_id', 'type', 'estimation', 'costItems'],
    cost_items: ['name', 'description', 'id', '_id', 'type', 'rate', 'quantity']
  }[req.params.collection];

  if (!targetFields) {
    res.status(404);
    return next(req.params.collection + ' not found.');
  }

  req.collectionFields = targetFields;
  next();
}
