module.exports = function getCollectionQuery(req, res, next) {
  req.collectionQuery = req.query.query || {};
  next();
};
