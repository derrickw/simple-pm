var express = require('express'),
  _ = require('lodash'),
  app = express(),
  bodyParser = require('body-parser'),
  simplePm = require('../lib');

module.exports = app;

var createPromiseLogger = function createPromiseLogger(logger) {
  return _.bind(function promiseLogger(results) {
    this.info.call(this, results);
    return results
  }, logger);
};

app.get('/', function(req, res) {
  res.send('welcome to the api spot');
});

app.use('/:collection', require('./middleware/get_collection'));
app.use('/:collection', require('./middleware/get_collection_fields'));

app.get('/:collection', require('./middleware/get_collection_query'), function(req, res) {
  var logger = simplePm.createLogger({
    name: 'GET Collection'
  });

  logger.info(req.collectionQuery);

  req.collection.find(req.collectionQuery)
    .then(handleFound)
    .catch(handleFindError);

  function handleFound(results) {
    logger.log('handleFound results', results);
    res.json(results);
  }

  function handleFindError(err) {
    logger.error('handleFindError', err.stack);
    res.status(500).send(err);
  }
});

app.get('/:collection/:collectionId', function(req, res) {
  var logger = simplePm.createLogger({
    name: 'GET Collection by ID'
  });

  var promiseLogger = _.bind(function promiseLogger(results) {
    this.info.call(this, 'PROMISE LOGGER', results);
    return results
  }, logger);

  logger.info(req.params.collectionId);

  req.collection.findOne(req.params.collectionId)
    .then(handleFound)
    .catch(handleFindError);

  function handleFound(results) {
    logger.log('handleFound results', results);
    res.json(results);
  }

  function handleFindError(err) {
    logger.error('handleFindError', err.stack);
    res.status(500).send(err);
  }
});

app.post('/:collection', bodyParser.json(), function(req, res) {
  var logger = simplePm.createLogger({
    name: 'POST Collection'
  });

  var logPromise = createPromiseLogger(logger);

  var fields = req.collectionFields;
  var postData = _.pick(req.body, fields);

  req.collection.save(postData)
    .then(logPromise)
    .then(handleSaved)
    .then(logPromise)
    .catch(handleSaveError);

  function handleSaved(saveResults) {
    res.json(saveResults);
  }

  function handleSaveError(err) {
    console.log('responding with error', err);
    res.status(500).send(JSON.stringify(err));
  }
});

app.put('/:collection', bodyParser.json(), function(req, res) {
  var logger = simplePm.createLogger({
    name: 'PUT Collection[' + req.params.collection + ']'
  });

  var logPromise = createPromiseLogger(logger);

  var fields = req.collectionFields;
  var updateData = _.pick(req.body, fields);

  logger.info('updateData', updateData);

  req.collection.save(updateData)
    .then(logPromise)
    .then(handleSaved)
    .then(logPromise)
    .catch(handleSaveError);

  function handleSaved(saveResults) {
    res.json(saveResults);
  }

  function handleSaveError(err) {
    res.status(500).send(err);
  }
});

app.delete('/:collection/:coll_id', bodyParser.json(), function(req, res) {

  var targetId = req.params.coll_id;

  req.collection.deactivate(targetId)
    .then(handleDeleted)
    .catch(handleDeleteError);

  function handleDeleted(deleteResults) {
    res.json(deleteResults);
  }

  function handleDeleteError(err) {
    res.status(500).send(err);
  }

});

app.use('/:collection', function(err, req, res) {
  console.log(err);
  res.send('collection route error');
});
